##
## aPackage sfx archiver
##
## aPackage makefile (NMAKE)
##
## Copyright 2001-2014 Joergen Ibsen
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

target	= aPackage.exe
objs	= aPackage.obj encrypt.obj
libs	= aplib.lib

cflags	= /nologo /W2 /O2 /EHsc /GS /GL /c
cvars	= /DNDEBUG
lflags	= /nologo /release /subsystem:console,5.01 /ltcg

all: $(target)

.cpp.obj:
	$(CXX) $(cdebug) $(cflags) $(cvars) $<

$(target): $(objs)
	link $(ldebug) $(lflags) /out:$@ $** $(libs)

clean:
	del /Q $(objs) $(target)

# dependencies
aPackage.obj : aplib.h encrypt.hpp stub\sfxstub.h
encrypt.obj : encrypt.hpp
