//
// aPackage sfx archiver
//
// TEA encryption and CRC32
//
// Copyright 2001-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef ENCRYPT_HPP_INCLUDED
#define ENCRYPT_HPP_INCLUDED

#include <cstdlib>

void tea_encrypt(unsigned int *v, unsigned int *d, unsigned int *k);

void tea_decrypt(unsigned int *v, unsigned int *d, unsigned int *k);

unsigned int crc32mem(unsigned char *buffer, unsigned int len);

void generatekey(unsigned char *key, unsigned int *keydata);

int encryptdata(unsigned char *buffer, int length, unsigned int *keydata);

unsigned char randbyte();

#endif // ENCRYPT_HPP_INCLUDED
