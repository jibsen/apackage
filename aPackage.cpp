//
// aPackage sfx archiver
//
// main code v1.15
//
// Copyright 2001-2014 Joergen Ibsen
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#define APSFX_RELEASE

#include <windows.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <climits>
#include <fcntl.h>
#include <errno.h>
#include <io.h>
#include <conio.h>
#include <sys/stat.h>

#include "aplib.h"
#include "encrypt.hpp"

#include <algorithm>
#include <functional>
#include <vector>
#include <list>

using namespace std;

unsigned char stub[] = {

#include "stub/sfxstub.h"

};
const int stubsize = sizeof (stub);

// program version
const unsigned int AP_VERSION = 112;

// flags
const unsigned int APSFX_OVERWRITE = 1;
const unsigned int APSFX_SILENT    = 2;
const unsigned int APSFX_TMPPATH   = 4;
const unsigned int APSFX_AUTOQUIT  = 8;

// switches
int SWITCHl = 0; // list archive (not supported yet)

int SWITCHr = 0; // recurse sub-dirs
int SWITCHf = 1; // store empty folders
int SWITCHs = 0; // sort files by extension
int SWITCHp = 0; // password protect
int SWITCHo = 0; // do not default to overwrite
int SWITCHe = 0; // execute setup.exe
int SWITCHq = 0; // silent extraction
int SWITCHt = 0; // default extraction path (empty = temp)
int SWITCHa = 0; // automatically quit after succesfull extraction

// header (with 'AP32')
unsigned int aP32_header[4] = { 0x32335041, 0, 0, 0 };

const unsigned int dataheader_size = 32;

unsigned char *archivedata = 0;
unsigned char *packeddata = 0;

const char *run_after = "\0";
const char *default_path = "\0";
const char *clpassword = "\0";

int filelistdata_size = 0;
int filedata_size = 0;
int dirlistdata_size = 0;
int filenamedata_size = 0;

int additionaldata_size = 0;

int total_size = 0;
int packed_size = 0;

struct APFILE {
	// constructor
	APFILE(int pi, char *rn, int eo, int fs, FILETIME &ds, DWORD attr)
		: path_index(pi),
		  rel_name(rn),
		  ext_ptr(rn+eo),
		  size(fs),
		  date_stamp(ds),
		  attribs(attr & (FILE_ATTRIBUTE_ARCHIVE | FILE_ATTRIBUTE_HIDDEN |
		                  FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY |
		                  FILE_ATTRIBUTE_SYSTEM))
	{
		/* nothing */
	}

	// members
	int path_index;
	char *rel_name;
	char *ext_ptr;
	int size;
	FILETIME date_stamp;
	DWORD attribs;
};

vector<char *> path_prefixes;

list<APFILE *> flist;
list<char *> dirlist;

struct Cstring_less : binary_function<const char*,const char*,bool> {
	bool operator()(const char *lhs, const char *rhs) const
	{
		return(strcmp(lhs, rhs) < 0);
	}
};

struct Cstring_eq : binary_function<const char*,const char*,bool> {
	bool operator()(const char *lhs, const char *rhs) const
	{
		return(strcmp(lhs, rhs) == 0);
	}
};

struct APFILE_less : binary_function<const APFILE*,const APFILE*,bool> {
	bool operator()(const APFILE *lhs, const APFILE *rhs) const
	{
		return(strcmp(lhs->rel_name, rhs->rel_name) < 0);
	}
};

struct APFILE_ext_less : binary_function<const APFILE*,const APFILE*,bool> {
	bool operator()(const APFILE *lhs, const APFILE *rhs) const
	{
		return(strcmp(lhs->ext_ptr, rhs->ext_ptr) < 0);
	}
};

struct APFILE_eq : binary_function<const APFILE*,const APFILE*,bool> {
	bool operator()(const APFILE *lhs, const APFILE *rhs) const
	{
		return(strcmp(lhs->rel_name, rhs->rel_name) == 0);
	}
};

void apc_exit(int n)
{
	// exit with errorlevel = error no.
	exit(n);
}

void apc_error(const char *s)
{
	// print out error information
	printf("\n\n\007ERR: %s\n", s);

	apc_exit(1);
}

bool goodpassword(const char *str_)
{
	for (const unsigned char *str = (const unsigned char *)str_ ; *str; str++)
	{
		if ((*str < 0x20) || (*str > 0x7e)) return(false);
	}
	return(true);
}

int removeeol(char *str)
{
	char *strorg = str;

	// find eol
	while (*str) str++;

	// zero any trailing eol characters
	for (str--; (str >= strorg) && ((*str == 0x0d) || (*str == 0x0a)); str--) *str = 0;

	return (str - strorg + 1);
}

int getpassword(char *pwdstr, int maxlen)
{
	HANDLE hConIn = GetStdHandle(STD_INPUT_HANDLE);

	// get current console mode
	DWORD ConInMode;
	GetConsoleMode(hConIn, &ConInMode);

	// set console mode for password entry
	SetConsoleMode(hConIn, ENABLE_LINE_INPUT);

	// read and zero-terminate password
	DWORD Read = 0;
	ReadConsole(hConIn, pwdstr, maxlen - 1, &Read, NULL);
	pwdstr[Read] = 0;

	// set old console mode
	SetConsoleMode(hConIn, ConInMode);

	return (removeeol(pwdstr));
}

bool isproperdir(WIN32_FIND_DATA &fd)
{
	if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) return(false);

	if (strcmp(fd.cFileName, ".") == 0) return(false);
	if (strcmp(fd.cFileName, "..") == 0) return(false);

	return(true);
}

bool iswildcard(char *spec)
{
	for ( ; *spec; spec++) if ((*spec == '*') || (*spec == '?')) return(true);
	return(false);
}

void getfilepart(char *spec, char *fspec)
{
	char *where = strrchr(spec, '\\');

	if (where == NULL) where = spec;
	else where++;

	strcpy(fspec, where);
}

void getpathpart(char *spec, char *pspec)
{
	char *where = strrchr(spec, '\\');

	if (where)
	{
		strncpy(pspec, spec, where - spec + 1);
		pspec[where - spec + 1] = '\0';
	} else {
		pspec[0] = '\0';
	}
}

void addfile(int path_index, const char *rel_path, WIN32_FIND_DATA &fd)
{
//   printf("'%s' ++ '%s' ++ '%s'\n", path_prefixes[path_index], rel_path, fd.cFileName);

	char rel_name[MAX_PATH];

	// build relative path and name
	strcpy(rel_name, rel_path);
	strcat(rel_name, fd.cFileName);

	// find extension offset
	int ext_offs;
	{
		char *ext = rel_name + strlen(rel_path);

		ext = strrchr(ext, '.');

		if (ext == 0)
		{
			strcat(rel_name, ".");

			ext_offs = strlen(rel_name);

		} else {

			ext_offs = ext - rel_name + 1;
		}
	}

	flist.push_back(new APFILE(path_index, strdup(rel_name), ext_offs, fd.nFileSizeLow, fd.ftLastWriteTime, fd.dwFileAttributes));

	filedata_size += fd.nFileSizeLow;
}

void addfilesbyspec(int path_index, const char *rel_path, const char *fspec)
{
	WIN32_FIND_DATA fd;

	char spec[MAX_PATH];

	strcpy(spec, path_prefixes[path_index]);
	strcat(spec, rel_path);
	strcat(spec, fspec);

	HANDLE ffhandle = FindFirstFile(spec, &fd);

	if (ffhandle == INVALID_HANDLE_VALUE) return;

	BOOL havefile = TRUE;

	while (havefile)
	{
		if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			addfile(path_index, rel_path, fd);
		}

		havefile = FindNextFile(ffhandle, &fd);
	}

	FindClose(ffhandle);
}

void recursivelyaddfilesbyspec(int path_index, const char *rel_path, const char *fspec)
{
	// add matching files in the base directory of the spec
	addfilesbyspec(path_index, rel_path, fspec);

	WIN32_FIND_DATA fd;

	char new_dirspec[MAX_PATH];

	strcpy(new_dirspec, path_prefixes[path_index]);
	strcat(new_dirspec, rel_path);
	strcat(new_dirspec, "*.*");

	HANDLE ffhandle = FindFirstFile(new_dirspec, &fd);

	if (ffhandle == INVALID_HANDLE_VALUE) return;

	BOOL havefile = TRUE;

	while (havefile)
	{
		if (isproperdir(fd))
		{
			char new_rel_path[MAX_PATH];

			// gather spec for subdirectory
			strcpy(new_rel_path, rel_path);
			strcat(new_rel_path, fd.cFileName);
			strcat(new_rel_path, "\\");

			unsigned int cur_num_files = flist.size();

			recursivelyaddfilesbyspec(path_index, new_rel_path, fspec);

			// if we are adding empty dirs (or dir was non-empty), add it
			if ((SWITCHf != 0) || (cur_num_files != flist.size()))
			{
				// add relative path to dirlist
				dirlist.push_back(strdup(new_rel_path));
			}
		}

		havefile = FindNextFile(ffhandle, &fd);
	}

	FindClose(ffhandle);
}

int addfiledata(const char *fname, unsigned char *buffer)
{
	int infile;

	if ((infile = open(fname, O_RDONLY | O_BINARY)) == -1)
		apc_error("unable to open input file");

	int infilesize = lseek(infile, 0, SEEK_END);

	lseek(infile, 0, SEEK_SET);

	read(infile, buffer, infilesize);

	close(infile);

	return(infilesize);
}

void syntax()
{
	printf("Licensed under the Apache License, Version 2.0.\n\n"
	       "  Syntax:    aPackage [options] <sfx file> [files ...]\n"
	       "\n"
	       "  General options:\n"
	       "    -p[pwd]  Password protect archive (empty = ask)\n"
	       "    -s       Sort files by extension\n"
	       "\n"
	       "  SFX options:\n"
	       "    -a       Automatically quit after extraction\n"
	       "    -e<file> Execute 'file' after extraction\n"
	       "    -o       Default to not overwriting files\n"
	       "    -q       Create silent sfx (no extraction dialog)\n"
	       "    -t[path] Default directory is 'path' (empty = temp)\n"
	       "\n"
	       "  File handling options:\n"
	       "    -f<+|->  Store empty folders [+]\n"
	       "    -r[+|-]  Recurse subdirectories [-]\n");
}

unsigned int ratio(unsigned int x, unsigned int y)
{
	if (x <= UINT_MAX / 100) x *= 100; else y /= 100;

	if (y == 0) y = 1;

	return x / y;
}

int __cdecl callback(unsigned int insize, unsigned int inpos, unsigned int outpos, void *cbparam)
{
	printf("\r- packing archive (%3d%% done)", ratio(inpos, insize));

#ifdef AP_HAS_CONIO
	// check for ESC-hit
	if (kbhit())
	{
		unsigned char ch = getch();
		if (ch == 0) ch = getch();
		if (ch == 27)
		{
			return (0); // abort packing
		}
	}
#endif

	return (1); // continue packing
}

// =========================================================================
//  MAIN
// =========================================================================

int main(int argc, char *argv[])
{
	int sfxfilearg = 0, infilearg = 0;

	// write name and copyright notice
	printf("aPackage sfx archiver v1.15\n"
	       "Copyright 2001-2014 Joergen Ibsen (www.ibsensoftware.com)\n\n");

#ifndef APSFX_RELEASE
	printf("THIS IS _NOT_ A RELEASE VERSION - PLEASE DON'T DISTRIBUTE!!!\n\n");
#endif

	// write syntax when not enough parameters
	if (argc < 2)
	{
		syntax();
		return(1);
	}

	// parse command line
	for (int i = 1; i < argc; i++)
	{
		char *arg = argv[i];
		if ((*arg == '-') || (*arg == '/'))
		{
			while (*(++arg))
			{
				switch (*arg)
				{
				case '?':
				case 'h':
				case 'H':
					syntax();
					return(1);
					break;
				case 'p':
				case 'P':
					// check if argument is present
					if (*(++arg))
					{
						// set pointer to string
						clpassword = arg;
						if ((strlen(clpassword) < 4) || (strlen(clpassword) > 64))
						{
							printf("ERR: password must be 4-64 characters long\n");
							exit(0);
						}
						if (!goodpassword(clpassword))
						{
							printf("ERR: password can contain ascii values 32-126\n");
							exit(0);
						}
						SWITCHp = 1;
					} else {
						SWITCHp = 2;
					}
					// skip rest of switch
					while (*(arg + 1)) ++arg;
					break;
				case 's':
				case 'S':
					SWITCHs = 1;
					break;
				case 'a':
				case 'A':
					SWITCHa = 1;
					break;
				case 'e':
				case 'E':
					// check if argument is present
					if (*(++arg))
					{
						// set pointer to string
						run_after = arg;
						SWITCHe = 1;
						// skip rest of switch
						while (*(arg + 1)) ++arg;
					} else {
						syntax();
						printf("ERR: must supply file to execute in '-e<file>'\n");
						exit(0);
					}
					break;
				case 'q':
				case 'Q':
					SWITCHq = 1;
					break;
				case 't':
				case 'T':
					// check if argument is present
					if (*(++arg))
					{
						// set pointer to string
						default_path = arg;
						SWITCHt = 1;
					} else {
						SWITCHt = 2;
					}
					// skip rest of switch
					while (*(arg + 1)) ++arg;
					break;
				case 'o':
				case 'O':
					SWITCHo = 1;
					break;
				case 'r':
				case 'R':
					switch (arg[1])
					{
					case '+' :
						if (infilearg == 0) SWITCHr = 1;
						arg++;
						break;
					case '-' :
						if (infilearg == 0) SWITCHr = 0;
						arg++;
						break;
					default  :
						if (infilearg == 0) SWITCHr = 2;
						break;
					}
					break;
				case 'f':
				case 'F':
					switch (arg[1])
					{
					case '+' :
						if (infilearg == 0) SWITCHf = 1;
						arg++;
						break;
					case '-' :
						if (infilearg == 0) SWITCHf = 0;
						arg++;
						break;
					default  :
						syntax();
						printf("ERR: unknown switch '-f%c'\n", arg[1]);
						exit(0);
					}
					break;
				default :
					syntax();
					printf("ERR: unknown switch '-%c'\n", *arg);
					exit(0);
				}
			}
		} else {
			if (sfxfilearg != 0)
			{
				if (infilearg == 0) infilearg = i;
			} else sfxfilearg = i;
		}
	}

	// print syntax if no sfx-file
	if (sfxfilearg == 0)
	{
		syntax();
		return(1);
	}

	if (SWITCHl)
	{
		printf("Listing archives is not supported yet.\n");
		return(1);

	} else {

		if (infilearg == 0)
		{
			printf("No archive, or no input files specified.\n");
			return(1);
		}

		printf("- scanning files ... ");

		// add all files to archive data
		for ( ; infilearg < argc; infilearg++)
		{
			if ((*argv[infilearg] == '-') || (*argv[infilearg] == '/'))
			{
				char *arg = argv[infilearg];
				while (*(++arg))
				{
					switch (*arg)
					{
					case 'r':
					case 'R':
						switch (arg[1])
						{
						case '+' :
							SWITCHr = 1;
							arg++;
							break;
						case '-' :
							SWITCHr = 0;
							arg++;
							break;
						default  :
							SWITCHr = 2;
						}
						break;
					case 'f':
					case 'F':
						switch (arg[1])
						{
						case '+' :
							SWITCHf = 1;
							arg++;
							break;
						case '-' :
							SWITCHf = 0;
							arg++;
							break;
						}
						break;
					}
				}

			} else {

				// get filespec
				char fspec[MAX_PATH];
				getfilepart(argv[infilearg], fspec);

				if (strlen(fspec) != 0)
				{
					// add absolute path prefix to list
					{
						char pspec[MAX_PATH];

						getpathpart(argv[infilearg], pspec);

						path_prefixes.push_back(strdup(pspec));
					}

					// add files to filelist
					switch (SWITCHr)
					{
					case 0:
						addfilesbyspec(path_prefixes.size() - 1, "", fspec);
						break;
					case 1:
						recursivelyaddfilesbyspec(path_prefixes.size() - 1, "", fspec);
						break;
					case 2:
						if (iswildcard(fspec))
							recursivelyaddfilesbyspec(path_prefixes.size() - 1, "", fspec);
						else
							addfilesbyspec(path_prefixes.size() - 1, "", fspec);
						break;
					}
				}
			}
		}

		int num_files = flist.size();

		if (num_files == 0)
		{
			printf("0 files found, aborting.\n");
			return 1;
		}

		// sort filelist and remove duplicates
		flist.sort(APFILE_less());
		flist.unique(APFILE_eq());

		if (SWITCHs)
		{
			// sort file list by extension
			flist.sort(APFILE_ext_less());
		}

		// calculate size of filelist data
		if (flist.size() != num_files)
		{
			int old_num_files = num_files;

			num_files = flist.size();

			filelistdata_size = (num_files * 24) + 8;

			printf("%d file(s) (%d kb) (%d duplicate filename(s) removed)\n", num_files, (filedata_size+1023)/1024, old_num_files - num_files);

		} else {

			filelistdata_size = (num_files * 24) + 8;

			printf("%d file(s) (%d kb)\n", num_files, (filedata_size+1023)/1024);
		}

		printf("- analyzing data\n");

		// calculate size of dirlist data
		dirlistdata_size = 0;

		if (!dirlist.empty())
		{
			// sort dirlist and remove duplicates
			dirlist.sort(Cstring_less());
			dirlist.unique(Cstring_eq());

			dirlistdata_size = 1;

			// calculate size of dirlist data
			for (list<char *>::const_iterator p = dirlist.begin(); p != dirlist.end(); p++)
			{
				dirlistdata_size += strlen(*p) + 1;
			}
		}

		// calculate size of filename data
		filenamedata_size = 0;

		// calculate size of filename data
		for (list<APFILE *>::const_iterator p = flist.begin(); p != flist.end(); p++)
		{
			filenamedata_size += strlen((*p)->rel_name) + 1;
		}

		// calculate size of additional data
		additionaldata_size = 0;
		if (SWITCHe != 0) additionaldata_size += strlen(run_after) + 1;
		if (SWITCHt == 1) additionaldata_size += strlen(default_path) + 1;

		// calculate total data size
		total_size = dataheader_size + filelistdata_size + filedata_size +
		             dirlistdata_size + filenamedata_size + additionaldata_size;

		// warn if more than 20mb of data
		if (total_size > 20*1024*1024)
		{
			printf("    - warning: more than 20 mb data\n");
		}

		printf("- creating archive\n");

		archivedata = new unsigned char[total_size];

		{
			unsigned int *archivedata_int = (unsigned int *)archivedata;

			// set values in data header
			archivedata_int[0] = AP_VERSION;      // version number
			archivedata_int[1] = APSFX_OVERWRITE; // flags
			archivedata_int[2] = 0;               // not used yet
			archivedata_int[3] = 0;               // def_path_offs (0 if not used)
			archivedata_int[4] = 0;               // run_after_offs (0 if not used)
			archivedata_int[5] = 0;               // not used yet
			archivedata_int[6] = 0;               // not used yet
			archivedata_int[7] = 0;               // not used yet

			// set flags accoring to switches
			if (SWITCHo != 0) archivedata_int[1] &= !APSFX_OVERWRITE;
			if (SWITCHq != 0) archivedata_int[1] |= APSFX_SILENT;
			if (SWITCHt == 2) archivedata_int[1] |= APSFX_TMPPATH;
			if (SWITCHa != 0) archivedata_int[1] |= APSFX_AUTOQUIT;

			// set offset to dirlist
			archivedata_int[dataheader_size/4] = 0;
			if (dirlistdata_size) archivedata_int[dataheader_size/4] = dataheader_size + filelistdata_size + filedata_size;

			// add files
			unsigned int *filedataptr = archivedata_int + (dataheader_size/4) + 1;
			int data_offs = dataheader_size + filelistdata_size;
			int fname_offs = dataheader_size + filelistdata_size + filedata_size + dirlistdata_size;
			for (list<APFILE *>::const_iterator p = flist.begin(); p != flist.end(); p++)
			{
				char fname[MAX_PATH];

				// get full path and filename
				strcpy(fname, path_prefixes[(*p)->path_index]);
				strcat(fname, (*p)->rel_name);

				int size = addfiledata(fname, archivedata + data_offs);

				filedataptr[0] = fname_offs;
				filedataptr[1] = data_offs;
				filedataptr[2] = size;
				filedataptr[3] = (*p)->date_stamp.dwLowDateTime;
				filedataptr[4] = (*p)->date_stamp.dwHighDateTime;
				filedataptr[5] = (*p)->attribs;

				data_offs += size;
				fname_offs += strlen((*p)->rel_name) + 1;
				filedataptr += 6;
			}
			filedataptr[0] = 0;

			// add dirlist data
			unsigned char *dirlistptr = archivedata + dataheader_size + filelistdata_size + filedata_size;
			for (list<char *>::const_iterator p = dirlist.begin(); p != dirlist.end(); p++)
			{
				strcpy((char *)dirlistptr, *p);
				dirlistptr += strlen(*p) + 1;
			}

			// add filename data
			unsigned char *filenameptr = archivedata + dataheader_size + filelistdata_size + filedata_size + dirlistdata_size;
			for (list<APFILE *>::const_iterator p = flist.begin(); p != flist.end(); p++)
			{
				strcpy((char *)filenameptr, (*p)->rel_name);
				filenameptr += strlen((*p)->rel_name) + 1;
			}

			// add additional data
			unsigned char *additionalptr = archivedata + dataheader_size + filelistdata_size + filedata_size + dirlistdata_size + filenamedata_size;
			int additional_offs = dataheader_size + filelistdata_size + filedata_size + dirlistdata_size + filenamedata_size;
			if (SWITCHe != 0)
			{
				archivedata_int[4] = additional_offs;
				strcpy((char *)additionalptr, run_after);
				additionalptr += strlen(run_after) + 1;
				additional_offs += strlen(run_after) + 1;
			}
			if (SWITCHt == 1)
			{
				archivedata_int[3] = additional_offs;
				strcpy((char *)additionalptr, default_path);
				additionalptr += strlen(default_path) + 1;
				additional_offs += strlen(default_path) + 1;
			}
		}

		// pack archive data

		packeddata = new unsigned char[aP_max_packed_size(total_size)];

		unsigned char *workmem = new unsigned char[aP_workmem_size(total_size) + 16];

		packed_size = aP_pack(archivedata, packeddata, total_size, workmem, callback, NULL);

		printf("\r- packing archive (%d -> %d)\n", total_size, packed_size);

		delete[] workmem;
		delete[] archivedata;

		if (SWITCHp)
		{
			// use clock as random seed
			srand(time(NULL));

			char passwd[64];
			char passwdcheck[64];

			int passwd_length = 0;

			if (SWITCHp == 2)
			{
				printf("- password must be 4-64 characters long, and can contain ascii values 32-126\n");

				// get a valid password twice
				do {
					do {
						memset(passwd, 0, sizeof(passwd));

						printf("\rEnter password    : ");
						passwd_length = getpassword(passwd, 64);

						if (!goodpassword(passwd)) passwd_length = 0;
					} while (passwd_length < 4);

					memset(passwdcheck, 0, sizeof(passwdcheck));

					printf("\rRe-enter password : ");
					getpassword(passwdcheck, 64);
				} while (strcmp(passwd, passwdcheck) != 0);

			} else {

				strcpy(passwd, clpassword);
			}

			printf("\r- encrypting archive        \n");

			unsigned int key[4];

			// generate a key from the password
			generatekey((unsigned char *)passwd, key);

			memset(passwd, 0, sizeof(passwd));
			memset(passwdcheck, 0, sizeof(passwdcheck));

			// extend packed data to a multiple of 8 bytes
			while (packed_size & 7) packeddata[packed_size++] = randbyte();

			// set crc in header
			aP32_header[2] = crc32mem(packeddata, packed_size);

			// encrypt packed data using the key
			packed_size = encryptdata(packeddata, packed_size, key);

			// set values in header
			aP32_header[0] = 0x78785041;
			aP32_header[1] = packed_size + sizeof(aP32_header) - 8;
			aP32_header[3] = total_size;

			// set header in stub
			unsigned int *stub_int = (unsigned int *)stub;
			stub_int[12] = 0x474b5061;
			stub_int[13] = stubsize;
			stub_int[14] = packed_size + sizeof(aP32_header);

			memset(key, 0, sizeof(key));

		} else {

			aP32_header[1] = packed_size + sizeof(aP32_header);
			aP32_header[2] = crc32mem(packeddata, packed_size);
			aP32_header[3] = total_size;

			// set header in stub
			unsigned int *stub_int = (unsigned int *)stub;
			stub_int[12] = 0x474b5061;
			stub_int[13] = stubsize;
			stub_int[14] = packed_size + sizeof(aP32_header);
		}

		printf("- writing archive\n");

		int outfile;

		if ((outfile = open(argv[sfxfilearg], O_WRONLY | O_CREAT | O_BINARY | O_TRUNC, S_IREAD | S_IWRITE)) == -1)
			apc_error("unable to create sfx file");

		write(outfile, stub, stubsize);

		write(outfile, (unsigned char *)aP32_header, sizeof(aP32_header));

		write(outfile, packeddata, packed_size);

		close(outfile);

		delete[] packeddata;

		printf("\nAll done.\n");
	}

	// success
	return(0);
}
