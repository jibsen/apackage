
aPackage sfx archiver
=====================

Copyright 2001-2014 Joergen Ibsen

<http://www.ibsensoftware.com/>


Introduction
------------

aPackage is a simple sfx archiver developed as an alternative to the well-
known archive formats (zip, rar, ace) which produce small archives but
require a large sfx stub to depack the data. For smaller sets of files
the size of the sfx stub can account for quite a large part of the final
sfx size.

aPackage uses LZ77 based compression (the aPLib compression library) to
produce tight archives while maintaining a very small stub size. The sfx
archives also benefit from the extremely fast extraction speed of the aPLib
library.

aPackage sfx archives support many features known from other sfx archives
like password protection, optional overwriting of files, optional running
of a setup file after extraction.

Due to the way the sfx stub works it is not recommended to use aPackage
for archiving more than 20 mb of data.


Syntax
------

These are the arguments for the aPackage archiver:

    Syntax:    aPackage [options] <sfx file> [files ...]

    General options:
      -p[pwd]  Password protect archive (empty = ask)
      -s       Sort files by extension

    SFX options:
      -a       Automatically quit after extraction
      -e<file> Execute 'file' after extraction
      -o       Default to not overwriting files
      -q       Create silent sfx (no extraction dialog)
      -t[path] Default directory is 'path' (empty = temp)

    File handling options:
      -f<+|->  Store empty folders [+]
      -r[+|-]  Recurse subdirectories [-]

Wildcards (`*` and `?`) are allowed in the file specifications.

The switches can be given anywhere in the argument-list, in any case and
in any order you like. File handling options affect all file specifications
that follow them. The switches work like this:

    -p[pwd]  : Encrypts the archive with a password (4-64 characters long).
               If no password is given, the sfx will ask for it.

    -s       : Sorts the files by extension before adding them to the
               archive. May improve compression ratio in some cases.

    -a       : Makes the sfx stub automatically quit after successful
               extraction.

    -e<file> : Makes the sfx stub execute the file 'file' after extraction
               (can be used to do post-extraction stuff like installing).

    -o       : By default the 'Overwrite files' checkbox in the sfx is
               checked, this switch makes it default to being un-checked.

    -q       : Creates a silent sfx archive, which will extract the files to
               the current directory (or the temp directory if '-t' is
               specified) without showing the extraction dialog.

    -t[path] : Sets the default extraction path to 'path'. If no path is
               given, the temp directory is used.

    -r[+|-]  : Recurses subdirectories searching for files that match the
               file specifications.

                 -r  : recurse only on wildcards
                 -r+ : recurse always
                 -r- : do not recurse [default]

    -f<+|->  : Makes the archiver store empty directories in the archive.

                 -f+ : store empty directories [default]
                 -f- : do not store empty directories


Tips and Tricks
---------------

When creating an archive with many files of different types, the `-s`
switch can reduce the size of the resulting archive substantially.

If you are making an installer, the switch combination `-q -t -e"setup.exe"`
can be used to make a sfx archive which quietly depacks the contents to the
temp directory and runs the file `setup.exe`.
