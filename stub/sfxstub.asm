;;
;; aPackage sfx archiver
;;
;; Win32 sfx stub
;;
;; Copyright 2001-2014 Joergen Ibsen
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
;;

; This code was written with great help from Iczelion's win32
; assembly tutorials

; -- Version history (releases are marked with a '*') --
;
;   v1.15 *: Converted to FASM syntax, updated aPLib code.
;   v1.14 *: Changed URL, version update.
;   v1.13  : Version update.
;   v1.12 *: Added support for using a default path.
;   v1.11  : Fixed another Win2k compatibility bug.
;   v1.10  : Added support for running a file given in the data.
;   v1.09  : Added code to set file attributes.
;   v1.08  : Win2k compatibility bugs fixed.
;   v1.07  : Fixed the dialog procedures.
;   v1.06 *: Added support for using tmp dir as default.
;   v1.05  : Added support for silent extraction.
;   v1.04  : Added support for running a setup file.
;   v1.03  : Added code to set file date and time.
;   v1.02  : Added TEA decryption.
;   v1.01  : Added crc checking.
;   v1.00  : Initial version. [August 19th 2001]

format PE GUI 4.0

entry __ENTRY32

include 'win32ax.inc'

; IDs used in resources
IDI_MAINICON    equ 500

IDD_MAIN        equ 2000
IDD_ABOUT       equ 2001
IDD_PASSWD      equ 2002

IDC_EXTRACTDIR  equ 2500
IDC_BROWSE      equ 2501
IDC_OVERWRITE   equ 2502
IDC_RUNSETUP    equ 2503
IDC_ABOUT       equ 2504
IDC_EXTRACT     equ 2505
IDC_QUIT        equ 2506
IDC_STATUS      equ 2507
IDC_LINK        equ 2508
IDC_PASSWORD    equ 2509

; a few equates missing from the fasm includes
BIF_RETURNONLYFSDIRS  equ 0x00000001
BIF_DONTGOBELOWDOMAIN equ 0x00000002

ERROR_ALREADY_EXISTS equ 183
ERROR_PATH_NOT_FOUND equ 3

DM_SETDEFID equ (WM_USER+1)

; structure definitions
struct APEXEHEADER
	tag            dd ?
	data_offset    dd ?
	data_size      dd ?
ends

struct APPACKEDHEADER
	tag            dd ?
	packed_size    dd ?
	crc32          dd ?  ; crc32 of the packed, unencrypted data
	orig_size      dd ?
ends

struct APDATAHEADER
	version        dd ?
	flags          dd ?
	message_offs   dd ? ; not used yet, must be 0
	def_path_offs  dd ? ; 0 if not used
	run_after_offs dd ? ; 0 if not used
	reserved1      dd ? ; not used yet, must be 0
	reserved2      dd ? ; not used yet, must be 0
	reserved3      dd ? ; not used yet, must be 0
ends

struct APFILEHEADER
	fname_offs     dd ?
	data_offs      dd ?
	data_length    dd ?
	ftime_1        dd ?
	ftime_2        dd ?
	fattribs       dd ?
ends

; flags
APSFX_OVERWRITE equ 1
APSFX_SILENT    equ 2
APSFX_TMPPATH   equ 4
APSFX_AUTOQUIT  equ 8

; =============================================================

section '.text' code readable executable

__ENTRY32:

	invoke  GetModuleHandle, NULL
	mov     [hInstance], eax

	; get command line
	invoke  GetCommandLine
	mov     [CommandLine], eax

	; get filename of sfx executable
	invoke  GetModuleFileName, [hInstance], szFileName, 512

	; read and depack archive data
	stdcall ReadAPData, szFileName

	mov     [lpAPData], eax

	.if ~eax
		invoke  MessageBox, NULL, szFileErr, AppName, MB_OK + MB_ICONERROR
		invoke  ExitProcess, 1
	.endif

	; get flags
	mov     edx, [lpAPData]
	mov     eax, [edx + APDATAHEADER.flags]
	mov     [uFlags], eax

	; check overwrite flag
	test    [uFlags], APSFX_OVERWRITE
	.if ZERO?
		mov     [Overwrite], 0
	.endif

	; check autoquit flag
	test    [uFlags], APSFX_AUTOQUIT
	.if ~ZERO?
		mov     [AutoQuit], 1
	.endif

	; check run setup entry
	mov     edx, [lpAPData]
	mov     eax, [edx + APDATAHEADER.run_after_offs]
	.if eax
		add     eax, edx
		mov     [lpSetupExe], eax
		mov     [HaveRunSetup], 1
		mov     [RunSetupExe], 1
	.endif

	; check tmp path flag
	test    [uFlags], APSFX_TMPPATH
	.if ~ZERO?
		invoke  GetTempPath, MAX_PATH, szExtractPath
		.if ~eax
			invoke  lstrcpy, szExtractPath, szCRootDir
		.endif
	.else
		invoke  GetCurrentDirectory, MAX_PATH, szExtractPath
		.if ~eax
			invoke  lstrcpy, szExtractPath, szCRootDir
		.endif
	.endif

	; check default path entry
	mov     edx, [lpAPData]
	mov     eax, [edx + APDATAHEADER.def_path_offs]
	.if eax
		add     eax, edx
		invoke  lstrcpy, szExtractPath, eax
	.endif

	; check silent flag
	test    [uFlags], APSFX_SILENT
	.if ZERO?
		; show main dialog
		invoke  DialogBoxParam, [hInstance], IDD_MAIN, 0, MainDlgProc, 0

		jmp     free_return_success
	.endif

	; make sure path is terminated by a backslash
	stdcall TerminatePath, szExtractPath

	; create path if it does not exist, and set current dir to it
	stdcall CreateSetPath, szExtractPath

	.if ~eax
		mov     eax, szPathErr
		jmp     free_return_failure
	.endif

	; create the required directories
	stdcall CreatePaths, [lpAPData]

	.if ~eax
		mov     eax, szDirErr
		jmp     free_return_failure
	.endif

	; perform extraction of files
	stdcall ExtractFiles, [lpAPData]

	.if ~eax
		mov     eax, szExtrFileErr
		jmp     free_return_failure
	.endif

	cmp     [RunSetupExe], 0
	je      free_return_success

	; run setup.exe
	invoke  ShellExecute, NULL, szShellOpen, [lpSetupExe], NULL, NULL, SW_SHOWNORMAL

	cmp     eax, 32
	jg      free_return_success

	mov     eax, szRunSetupErr
	jmp     free_return_failure

free_return_failure:
	invoke  MessageBox, NULL, eax, AppName, MB_OK or MB_ICONERROR

	; return 1 = error
	mov     eax, 1
	jmp     free_return_eax

free_return_success:
	; return 0 = ok
	xor     eax, eax

free_return_eax:
	; get handle of AP memory, unlock and free
	push    eax
	invoke  GlobalHandle, [lpAPData]
	mov     [hAPData], eax
	.if eax
		invoke  GlobalUnlock, [hAPData]
		invoke  GlobalFree, [hAPData]
	.endif
	pop     eax

	; exit, return eax
	invoke  ExitProcess, eax

; ===================================================================

proc BrowseForFolder hWndOwner, lpBuffer, lpTitle

	local   bi:BROWSEINFO
	local   lpIDList:DWORD

	push    [hWndOwner]
	pop     [bi.hwndOwner]
	mov     [bi.pidlRoot], NULL
	mov     [bi.pszDisplayName], NULL
	push    [lpTitle]
	pop     [bi.lpszTitle]
	mov     [bi.ulFlags], BIF_RETURNONLYFSDIRS or BIF_DONTGOBELOWDOMAIN
	mov     [bi.lpfn], NULL
	mov     [bi.lParam], NULL
	mov     [bi.iImage], 0

	invoke  SHBrowseForFolder, addr bi
	mov     [lpIDList], eax

	.if eax
		invoke  SHGetPathFromIDList, [lpIDList], [lpBuffer]
		push    eax
		invoke  CoTaskMemFree, [lpIDList]
		pop     eax
	.endif

	ret

endp

; ===================================================================

proc TerminatePath lpPath

	mov     edx, [lpPath]
	mov     ax, '\'
	dec     edx

@@:
	inc     edx
	cmp     byte [edx], ah
	jne     @b

	cmp     byte [edx-1], al
	je      @f

	mov     [edx], ax

@@:
	ret

endp

; ===================================================================

proc CreateSetPath lpPath

	local   tmppath[MAX_PATH + 1]:BYTE

	; attempt to create directory
	invoke  CreateDirectory, [lpPath], NULL

	test    eax, eax
	jnz     .setdir_return

	invoke  GetLastError

	cmp     eax, ERROR_ALREADY_EXISTS
	je      .setdir_return

	cmp     eax, ERROR_PATH_NOT_FOUND
	jne     .return_eax

	mov     ecx, [lpPath]
	lea     edx, [tmppath]

.next:
	mov     al, [ecx]
	inc     ecx
	mov     [edx], al
	inc     edx
	test    al, al
	jz      .done
	cmp     al, '\'
	jne     .next
	mov     byte [edx], 0
	push    ecx
	push    edx
	invoke  CreateDirectory, addr tmppath, NULL
	pop     edx
	pop     ecx
	test    eax, eax
	jnz     .next
	invoke  GetLastError
	cmp     eax, ERROR_ALREADY_EXISTS
	je      .next

.done:

.setdir_return:
	invoke  SetCurrentDirectory, [lpPath]

.return_eax:
	ret

endp

; ===================================================================

proc ReadAPData uses esi edi, fname:DWORD

	local   hFile:DWORD
	local   bBytesRead:DWORD
	local   ApkgHeader:APEXEHEADER
	local   packed_offset:DWORD
	local   packed_size:DWORD
	local   data_size:DWORD
	local   hMemSrc:DWORD
	local   lpMemSrc:DWORD
	local   hMemDst:DWORD
	local   lpMemDst:DWORD
	local   passwd_len:DWORD

	; attempt to open exe file
	invoke  CreateFile, [fname], GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL

	mov     [hFile], eax

	cmp     eax, INVALID_HANDLE_VALUE
	je      .exit_with_error

	; read 12 bytes of header from offset 30h in exe
	invoke  SetFilePointer, [hFile], 30h, NULL, FILE_BEGIN
	invoke  ReadFile, [hFile], addr ApkgHeader, sizeof.APEXEHEADER, addr bBytesRead, NULL

	test    eax, eax
	jz      .close_exit_with_error
	cmp     [bBytesRead], sizeof.APEXEHEADER
	jne     .close_exit_with_error

	; check tag of exe header info
	mov     eax, [ApkgHeader.tag]

	cmp     eax, 'aPKG'
	jne     .close_exit_with_error

	; get offset of data
	mov     eax, [ApkgHeader.data_offset]
	mov     [packed_offset], eax

	; get size of data
	mov     eax, [ApkgHeader.data_size]
	mov     [packed_size], eax

	; allocate mem for packed data
	invoke  GlobalAlloc, GMEM_MOVEABLE, [packed_size]

	mov     [hMemSrc], eax

	test    eax, eax
	jz      .close_exit_with_error

	invoke  GlobalLock, [hMemSrc]

	mov     [lpMemSrc], eax

	test    eax, eax
	jz      .close_exit_with_error

	; lseek to packed_offset
	invoke  SetFilePointer, [hFile], [packed_offset], NULL, FILE_BEGIN

	; attempt to read packed_size bytes
	invoke  ReadFile, [hFile], [lpMemSrc], [packed_size], addr bBytesRead, NULL

	; check size
	mov     eax, [bBytesRead]

	cmp     eax, [packed_size]
	jne     .close_exit_with_error

	invoke  CloseHandle, [hFile]

	; point esi to packed data
	mov     esi, [lpMemSrc]

	; check tag of packed data
	mov     eax, [esi + APPACKEDHEADER.tag]
	cmp     eax, 'APxx'
	jne     .not_encrypted

	; data is encrypted, so decrypt it

	; show get password dialog
	push    esi
	invoke  DialogBoxParam, [hInstance], IDD_PASSWD, [hMainDlg], PasswdDlgProc, 0
	mov     [passwd_len], eax
	pop     esi

	test    eax, eax
	jz      .exit_with_error

	mov     ecx, [packed_size]
	sub     ecx, sizeof.APPACKEDHEADER
	mov     eax, [lpMemSrc]
	add     eax, sizeof.APPACKEDHEADER
	stdcall DecryptData, eax, ecx, addr szPassword, [passwd_len]

	test    eax, eax
	jz      .exit_with_error

	; clear password buffer
	push    edi
	mov     edi, szPassword
	xor     eax, eax
	mov     ecx, 128 / 4
	rep     stosd
	pop     edi

	; adjust packed_size for the CBC decryption
	sub     [packed_size], 8

	jmp     .tag_ok

.not_encrypted:
	cmp     eax, 'AP32'
	jne     .exit_with_error

.tag_ok:
	; check packed_size
	mov     eax, [esi + APPACKEDHEADER.packed_size]
	cmp     eax, [packed_size]
	jne     .exit_with_error

	; calculate crc of packed, unencrypted data
	mov     ecx, [packed_size]
	sub     ecx, sizeof.APPACKEDHEADER
	mov     eax, [lpMemSrc]
	add     eax, sizeof.APPACKEDHEADER
	stdcall is_crc32_asm, eax, ecx

	; check crc against value in header
	cmp     eax, [esi + APPACKEDHEADER.crc32]
	jne     .exit_with_error

	; get unpacked size
	mov     eax, [esi + APPACKEDHEADER.orig_size]
	mov     [data_size], eax

	; allocate mem for unpacked data
	invoke  GlobalAlloc, GMEM_MOVEABLE, [data_size]

	mov     [hMemDst], eax

	test    eax, eax
	jz      .exit_with_error

	invoke  GlobalLock, [hMemDst]

	mov     [lpMemDst], eax

	test    eax, eax
	jz      .exit_with_error

	; unpack
	mov     esi, [lpMemSrc]
	add     esi, sizeof.APPACKEDHEADER
	stdcall aP_depack_asm_fast, esi, [lpMemDst]

	cmp     eax, [data_size]
	jne     .exit_with_error

	; free source mem
	invoke  GlobalUnlock, [hMemSrc]
	invoke  GlobalFree, [hMemSrc]

	; return pointer to unpacked data
	mov     eax, [lpMemDst]
	jmp     .exit_ok

.close_exit_with_error:
	invoke  CloseHandle, [hFile]

.exit_with_error:
	xor     eax, eax

.exit_ok:
	ret

endp

; ===================================================================

proc MainDlgProc hDlg, wMsg, wParam, lParam

	local   hBExtract:DWORD
	local   buff[128]:BYTE

	cmp     [wMsg], WM_INITDIALOG
	je      .wminitdialog

	cmp     [wMsg], WM_CLOSE
	je      .wmclose

	cmp     [wMsg], WM_COMMAND
	je      .wmcommand

	mov     eax, FALSE
	jmp     .return_eax

.wminitdialog:

	; save handle to main dialog
	mov     eax, [hDlg]
	mov     [hMainDlg], eax

	; set default path in dialog
	invoke  SetDlgItemText, [hDlg], IDC_EXTRACTDIR, szExtractPath

	; set state of checkbox
	.if [Overwrite]
		invoke  SendDlgItemMessage, [hDlg], IDC_OVERWRITE, BM_SETCHECK, BST_CHECKED, 0
	.endif

	; remove run setup checkbox if not needed, otherwise check it
	.if ~[HaveRunSetup]
		invoke  GetDlgItem, [hDlg], IDC_RUNSETUP
		invoke  DestroyWindow, eax
	.endif

	; collect string to display next to checkbox
	invoke  wsprintf, addr buff, szSetupFormat, [lpSetupExe]

	invoke  SendDlgItemMessage, [hDlg], IDC_RUNSETUP, WM_SETTEXT, 0, addr buff

	invoke  SendDlgItemMessage, [hDlg], IDC_RUNSETUP, BM_SETCHECK, BST_CHECKED, 0

	jmp     .return_true

.wmclose:

	invoke  PostMessage, [hDlg], WM_COMMAND, IDC_QUIT, 0
	jmp     .return_true

.wmcommand:

	cmp     [wParam], IDC_BROWSE
	je      .idcbrowse

	cmp     [wParam], IDC_ABOUT
	je      .idcabout

	cmp     [wParam], IDC_QUIT
	je      .idcquit

	cmp     [wParam], IDC_EXTRACT
	je      .idcextract

	jmp     .return_true

.idcbrowse:

	; show browse for folder dialog
	stdcall BrowseForFolder, [hDlg], szExtractPath, SelectDir

	; if user selected a new path, set it
	.if eax
		invoke  SetDlgItemText, [hDlg], IDC_EXTRACTDIR, szExtractPath
	.endif

	jmp     .return_true

.idcabout:

	; show about dialog
	invoke  DialogBoxParam, [hInstance], IDD_ABOUT, [hDlg], AboutDlgProc, 0

	jmp     .return_true

.idcquit:

	; end the dialog, return 1
	invoke  EndDialog, [hDlg], 1

	jmp     .return_true

.idcextract:

	; get state of overwrite checkbox
	invoke  SendDlgItemMessage, [hDlg], IDC_OVERWRITE, BM_GETCHECK, 0, 0

	.if eax = BST_CHECKED
		mov     [Overwrite], 1
	.else
		mov     [Overwrite], 0
	.endif

	; get state of run setup checkbox
	.if [HaveRunSetup]
		invoke  SendDlgItemMessage, [hDlg], IDC_RUNSETUP, BM_GETCHECK, 0, 0

		.if eax = BST_CHECKED
			mov     [RunSetupExe], 1
		.else
			mov     [RunSetupExe], 0
		.endif
	.endif

	; get path from edit box
	invoke  GetDlgItemText, [hDlg], IDC_EXTRACTDIR, szExtractPath, MAX_PATH

	; make sure path is terminated by a backslash
	stdcall TerminatePath, szExtractPath

	; create path if it does not exist, and set current dir to it
	stdcall CreateSetPath, szExtractPath

	.if ~eax
		invoke  MessageBox, NULL, szPathErr, AppName, MB_OK or MB_ICONERROR

		jmp     .return_true
	.endif

	; status = extracting
	invoke  SetDlgItemText, [hDlg], IDC_STATUS, Status_Extract

	; disable extract button
	invoke  GetDlgItem, [hDlg], IDC_EXTRACT
	mov     [hBExtract], eax
	invoke  EnableWindow, [hBExtract], FALSE

	; create the required directories
	stdcall CreatePaths, [lpAPData]

	.if ~eax
		; show error message
		invoke  MessageBox, NULL, szDirErr, AppName, MB_OK or MB_ICONERROR

		; status = error
		invoke  SetDlgItemText, [hDlg], IDC_STATUS, Status_Error

		; enable extract button
		invoke  EnableWindow, [hBExtract], TRUE

		jmp     .return_true
	.endif

	; perform extraction of files
	stdcall ExtractFiles, [lpAPData]

	.if ~eax
		; show error message
		invoke  MessageBox, NULL, szExtrFileErr, AppName, MB_OK or MB_ICONERROR

		; status = error
		invoke  SetDlgItemText, [hDlg], IDC_STATUS, Status_Error

		; enable extract button
		invoke  EnableWindow, [hBExtract], TRUE

		jmp     .return_true
	.endif

	; status = done
	invoke  SetDlgItemText, [hDlg], IDC_STATUS, Status_Done

	; set quit button as default button
	invoke  SendMessage, [hDlg], DM_SETDEFID, IDC_QUIT, 0
	invoke  GetDlgItem, [hDlg], IDC_QUIT
	invoke  SetFocus, eax

	.if [RunSetupExe]
		; run setup.exe
		invoke  ShellExecute, NULL, szShellOpen, [lpSetupExe], NULL, NULL, SW_SHOWNORMAL

		.if eax <= 32
			; show error message
			invoke  MessageBox, NULL, szRunSetupErr, AppName, MB_OK or MB_ICONERROR

			jmp     .return_true
		.endif
	.endif

	.if [AutoQuit]
		invoke  PostMessage, [hDlg], WM_COMMAND, IDC_QUIT, 0
	.endif

.return_true:
	mov     eax, TRUE

.return_eax:
	ret

endp

; ===================================================================

proc AboutDlgProc hDlg, wMsg, wParam, lParam

	local   hStatic:DWORD

	cmp     [wMsg], WM_CLOSE
	je      .wmclose

	cmp     [wMsg], WM_COMMAND
	je      .wmcommand

	mov     eax, FALSE
	jmp     .return_eax

.wmclose:

	invoke  PostMessage, [hDlg], WM_COMMAND, IDOK, 0

	jmp     .return_true

.wmcommand:

	cmp     [wParam], IDOK
	je      .idok

	cmp     [wParam], IDC_LINK
	je      .idclink

	jmp     .return_true

.idok:
	; end the dialog
	invoke  EndDialog, [hDlg], [wParam]

	jmp     .return_true

.idclink:
	; open homepage
	invoke  ShellExecute, NULL, szShellOpen, szHomePageUrl, NULL, NULL, 0

	jmp     .return_true

.return_true:
	mov     eax, TRUE

.return_eax:
	ret

endp

; ===================================================================

proc PasswdDlgProc hDlg, wMsg, wParam, lParam

	local   hStatic:DWORD

	cmp     [wMsg], WM_INITDIALOG
	je      .wminitdialog

	cmp     [wMsg], WM_CLOSE
	je      .wmclose

	cmp     [wMsg], WM_COMMAND
	je      .wmcommand

	mov     eax, FALSE
	jmp     .return_eax

.wminitdialog:

	; set a limit on the number of characters for the password
	invoke  SendDlgItemMessage, [hDlg], IDC_PASSWORD, EM_SETLIMITTEXT, 64, 0

	jmp     .return_true

.wmclose:

	invoke  PostMessage, [hDlg], WM_COMMAND, IDCANCEL, 0

	jmp     .return_true

.wmcommand:

	cmp     [wParam], IDOK
	je      .idok

	cmp     [wParam], IDCANCEL
	je      .idcancel

	jmp     .return_true

.idok:
	; clear password buffer
	push    edi
	mov     edi, szPassword
	xor     eax, eax
	mov     ecx, 128 / 4
	rep     stosd
	pop     edi

	; get password from edit box
	invoke  GetDlgItemText, [hDlg], IDC_PASSWORD, szPassword, 64

	.if eax > 3
		; end the dialog, return length
		invoke  EndDialog, [hDlg], eax
	.endif

	jmp     .return_true

.idcancel:

	; end the dialog, return 0
	invoke  EndDialog, [hDlg], 0

	jmp     .return_true

.return_true:
	mov     eax, TRUE

.return_eax:
	ret

endp

; ===================================================================

proc TeaDecrypt uses ebx esi edi, lpData:DWORD, lpDest:DWORD, lpKey:DWORD

	mov     ecx, 0c6ef3720h ; 09e3779b9h*32
	mov     edi, [lpKey]

	mov     esi, [lpData]
	mov     ebx, [esi]     ; ebx = y
	mov     edx, [esi + 4] ; edx = z

	.while ecx

		; z -= ((y<<4 ^ y>>5) + y) ^ (sum + k[sum>>11 & 3]);
		mov     eax, ebx
		mov     esi, ebx
		shl     eax, 4
		shr     esi, 5
		xor     eax, esi
		add     eax, ebx

		mov     esi, ecx
		shr     esi, 11
		and     esi, 3
		mov     esi, [edi + esi*4]
		add     esi, ecx

		xor     eax, esi

		sub     edx, eax

		sub     ecx, 09e3779b9h

		; y -= ((z<<4 ^ z>>5) + z) ^ (sum + k[sum & 3]);
		mov     eax, edx
		mov     esi, edx
		shl     eax, 4
		shr     esi, 5
		xor     eax, esi
		add     eax, edx

		mov     esi, ecx
		and     esi, 3
		mov     esi, [edi + esi*4]
		add     esi, ecx

		xor     eax, esi

		sub     ebx, eax

	.endw

	mov     esi, [lpDest]
	mov     [esi], ebx
	mov     [esi + 4], edx

	ret

endp

; ===================================================================

proc DecryptData uses ebx esi edi, lpData:DWORD, bDataLength:DWORD, lpKey:DWORD, bKeyLength:DWORD

	local   keyhash[4]:DWORD
	local   keydata[4]:DWORD
	local   feedback[2]:DWORD

	; contract / expand the key data into 16 bytes in keydata[]

	; fix block to 64 bytes
	mov     esi, [lpKey]
	mov     eax, [bKeyLength]
	.if eax < 64
		mov     byte [esi + eax], 080h
		.if eax < 63
			mov     byte [esi + 63], al
		.endif
	.endif

	; set initial hash value
	mov     eax, 0BEEFA007h
	mov     [keyhash], eax
	mov     eax, 0FEEDABEEh
	mov     [keyhash + 4], eax

	mov     ecx, 16

.hash_key_loop:
	; get key material into key
	mov     eax, [esi]
	mov     [keyhash + 8], eax
	mov     eax, [esi + 4]
	mov     [keyhash + 12], eax

	; perform modified Davies-Meyer
	push    ecx
	stdcall TeaDecrypt, addr keyhash, addr keyhash, addr keyhash
	pop     ecx

	add     esi, 8

	cmp     ecx, 9
	jne     .check_hash_key_loop

	; get first 8 bytes of hashed key
	mov     eax, [keyhash]
	mov     [keydata], eax
	mov     eax, [keyhash + 4]
	mov     [keydata + 4], eax

	; reset key pointer
	mov     esi, [lpKey]

.check_hash_key_loop:
	dec     ecx
	jnz     .hash_key_loop

	; get last 8 bytes of hashed key
	mov     eax, [keyhash]
	mov     [keydata + 8], eax
	mov     eax, [keyhash + 4]
	mov     [keydata + 12], eax

	mov     esi, [lpData]
	mov     ecx, [bDataLength]

	; check that size is a multiple of 8
	xor     eax, eax
	test    ecx, 7
	jnz     .exit

	; decrypt first block(IV) into FBR
	push    ecx
	stdcall TeaDecrypt, esi, addr feedback, addr keydata
	pop     ecx

.next_block:
	; decrypt next block into current block
	lea     ebx, [esi + 8]
	push    ecx
	stdcall TeaDecrypt, ebx, esi, addr keydata
	pop     ecx

	; xor block with feedback
	mov     eax, [feedback]
	xor     [esi], eax
	mov     eax, [feedback + 4]
	xor     [esi + 4], eax

	add     esi, 8

	; copy next (encrypted) block into feedback register
	mov     eax, [esi]
	mov     [feedback], eax
	mov     eax, [esi + 4]
	mov     [feedback + 4], eax

	sub     ecx, 8
	ja      .next_block

	mov     eax, 1

.exit:
	ret

endp

; ===================================================================

proc CreatePaths lpData:DWORD

	; get pointer to directory list
	mov     ecx, [lpData]
	add     ecx, sizeof.APDATAHEADER
	mov     ecx, [ecx]
	xor     eax, eax
	inc     eax
	test    ecx, ecx
	jz      .exit
	add     ecx, [lpData]

.next:
	mov     edx, ecx
.scan:
	mov     al, [ecx]
	inc     ecx
	test    al, al
	jnz     .scan

	push    ecx
	invoke  CreateDirectory, edx,NULL
	pop     ecx

	test    eax, eax
	jnz     .createok

	push    ecx
	invoke  GetLastError
	pop     ecx
	cmp     eax, ERROR_ALREADY_EXISTS
	je      .createok

	xor     eax, eax
	jmp     .exit

.createok:
	cmp     byte [ecx], 0
	jne     .next

.exit:
	ret

endp

; ===================================================================

proc ExtractFiles uses ebx, lpData:DWORD

	local   bNumBytesWritten:DWORD
	local   hFile:DWORD
	local   lpFileName:DWORD
	local   lpFileData:DWORD
	local   bDataLength:DWORD
	local   bCreateType:DWORD

	; determine creation flags by overwrite choice
	.if [Overwrite]
		mov     [bCreateType], CREATE_ALWAYS
	.else
		mov     [bCreateType], CREATE_NEW
	.endif

	; get pointer to first file header
	mov     ecx, [lpData]
	add     ecx, 4 + sizeof.APDATAHEADER

.next_file:
	; get data for file from file header
	mov     eax, [ecx + APFILEHEADER.fname_offs]
	test    eax, eax
	jz      .exit_ok
	add     eax, [lpData]
	mov     [lpFileName], eax
	mov     eax, [ecx + APFILEHEADER.data_offs]
	add     eax, [lpData]
	mov     [lpFileData], eax
	mov     eax, [ecx + APFILEHEADER.data_length]
	mov     [bDataLength], eax

	; attempt to create file
	push    ecx
	invoke  CreateFile, [lpFileName], GENERIC_WRITE, 0, NULL, [bCreateType], FILE_ATTRIBUTE_NORMAL, NULL
	pop     ecx
	mov     [hFile], eax

	test    eax, eax
	jz      .exit_error

	; write to file
	push    ecx
	invoke  WriteFile, [hFile],[lpFileData],[bDataLength],addr bNumBytesWritten,NULL
	pop     ecx

	; check if we wrote the data ok
	mov     eax, [bNumBytesWritten]
	cmp     eax, [bDataLength]
	jne     .close_exit_error

	; set file time and date
	lea     ebx, [ecx + APFILEHEADER.ftime_1]
	push    ecx
	invoke  SetFileTime, [hFile],NULL,NULL,ebx
	pop     ecx

	test    eax, eax
	jz      .close_exit_error

	; close handle
	push    ecx
	invoke  CloseHandle, [hFile]
	pop     ecx

	; set file attributes
	mov     ebx, [ecx + APFILEHEADER.fattribs]
	push    ecx
	invoke  SetFileAttributes, [lpFileName],ebx
	pop     ecx

	test    eax, eax
	jz      .exit_error

	; point ecx to next file header
	add     ecx, sizeof.APFILEHEADER

	jmp     .next_file

.exit_ok:
	xor     eax, eax
	inc     eax
	jmp     .exit

.close_exit_error:
	invoke  CloseHandle, [hFile]

.exit_error:
	xor     eax, eax

.exit:
	ret

endp

; ===================================================================

proc is_crc32_asm uses esi edi, src, len

	mov     esi, [src]         ; esi -> buffer
	mov     ecx, [len]         ; ecx =  length

	sub     eax, eax           ; crc = 0

	test    esi, esi
	jz      .c_exit

	test    ecx, ecx
	jz      .c_exit

	dec     eax                ; crc = 0xffffffff

	mov     edi, is_crctab_n   ; edi -> crctab

.c_next_byte:
	xor     al, [esi]
	inc     esi

	mov     edx, 0x0f
	and     edx, eax

	shr     eax, 4

	xor     eax, [edi + edx*4]

	mov     edx, 0x0f
	and     edx, eax

	shr     eax, 4

	xor     eax, [edi + edx*4]

	dec     ecx
	jnz     .c_next_byte

	not     eax

.c_exit:
	ret

endp

; ===================================================================

macro getbitM
{
	local .stillbitsleft
	add     dl, dl
	jnz     .stillbitsleft
	mov     dl, [esi]
	inc     esi
	adc     dl, dl
.stillbitsleft:
}

macro domatchM reg
{
	push    esi
	mov     esi, edi
	sub     esi, reg
	rep     movsb
	pop     esi
}

macro getgammaM reg
{
	local .getmore

	mov     reg, 1
.getmore:
	getbitM
	adc     reg, reg
	getbitM
	jc      .getmore
}

; ===================================================================

proc aP_depack_asm_fast uses esi edi ebx ebp, src, dst

	mov     esi, [src]
	mov     edi, [dst]

	push    edi

	cld
	mov     dl, 80h

.literal:
	mov     al, [esi]
	add     esi, 1
	mov     [edi], al
	add     edi, 1

	mov     ebx, 2

.nexttag:
	getbitM
	jnc     .literal

	getbitM
	jnc     .codepair

	xor     eax, eax
	getbitM
	jnc     .shortmatch

	getbitM
	adc     eax, eax
	getbitM
	adc     eax, eax
	getbitM
	adc     eax, eax
	getbitM
	adc     eax, eax
	jz      .thewrite

	mov     ebx, edi
	sub     ebx, eax
	mov     al, [ebx]

  .thewrite:
	mov     [edi], al
	inc     edi

	mov     ebx, 2
	jmp     .nexttag

.codepair:
	getgammaM eax
	sub     eax, ebx
	mov     ebx, 1
	jnz     .normalcodepair

	getgammaM ecx
	domatchM ebp

	jmp     .nexttag

.normalcodepair:
	dec     eax

	shl     eax, 8
	mov     al, [esi]
	inc     esi

	mov     ebp, eax

	getgammaM ecx

	cmp     eax, 32000
	sbb     ecx, -1

	cmp     eax, 1280
	sbb     ecx, -1

	cmp     eax, 128
	adc     ecx, 0

	cmp     eax, 128
	adc     ecx, 0

	domatchM eax
	jmp     .nexttag

.shortmatch:
	mov     al, [esi]
	inc     esi

	xor     ecx, ecx
	db      0c0h, 0e8h, 001h
	jz      .donedepacking

	adc     ecx, 2

	mov     ebp, eax

	domatchM eax

	mov     ebx, 1
	jmp     .nexttag

.donedepacking:
	pop     ecx
	mov     eax, edi
	sub     eax, ecx

	ret

endp

; =============================================================

section '.rdata' data readable

AppName       db "aPackage Self-Extractor",0

; error and warning messages
szPathErr     db "Unable to create or set the specified path.",0
szFileErr     db "An error occurred while processing the archive.",0
szDirErr      db "An error occurred while creating directories.",0
szExtrFileErr db "An error occurred while extracting files.",0
szRunSetupErr db "An error occurred while running program.",0

szShellOpen   db "open",0
szHomePageUrl db "http://www.ibsensoftware.com/",0

Status_Extract db "extracting files, please wait",0
Status_Done    db "all done",0
Status_Error   db "an error occurred",0

szCRootDir    db "C:\",0

szSetupFormat db "&Run '%s' after extracting",0

SelectDir     db "Please select destination directory:",0

is_crctab_n dd 0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190
            dd 0x6b6b51f4, 0x4db26158, 0x5005713c, 0xedb88320, 0xf00f9344
            dd 0xd6d6a3e8, 0xcb61b38c, 0x9b64c2b0, 0x86d3d2d4, 0xa00ae278
            dd 0xbdbdf21c

; =============================================================

section '.data' data readable writeable

hMainDlg      dd 0

Overwrite     dd 1
OverwriteAll  dd 0

HaveRunSetup  dd 0
RunSetupExe   dd 0

AutoQuit      dd 0

hInstance     dd ?
CommandLine   dd ?

szFileName    db 512 dup (?)

szExtractPath db MAX_PATH + 1 dup (?)

lpSetupExe    dd ?

lpAPData      dd ?
hAPData       dd ?

uFlags        dd ?

szPassword    db 128 dup (?)

; ===================================================================

section '.idata' import data readable writeable

library kernel32, 'KERNEL32.DLL',\
        user32, 'USER32.DLL',\
        shell32, 'SHELL32.DLL',\
        ole32, 'OLE32.DLL'

include 'api\kernel32.inc'
include 'api\user32.inc'
include 'api\shell32.inc'

import ole32,\
	CoTaskMemFree, 'CoTaskMemFree'

; ===================================================================

section '.rsrc' data readable resource from 'rsrc.res'

; ===================================================================

section '.reloc' data readable discardable fixups

; ===================================================================
