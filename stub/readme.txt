
For an encrypted archive, after packing add anough bytes to make the
size a multiple of 8 and then perform the crc on this.

The packed_size in the APPACKEDHEADER must be set to 8 below the size
in the APEXEHEADER (because the CBC mode takes up 8 bytes).

